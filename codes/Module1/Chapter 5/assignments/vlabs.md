# Factorials
<br>
Although the factorial function has its roots in combinatorics, formulas involving factorials occur in many areas of mathematics.
There are n! different ways of arranging n distinct objects into a sequence, the permutations of those objects. Factorials occur in algebra for various reasons, such as via the already mentioned coefficients of the binomial formula, or through averaging over permutations for symmetrization of certain operations. Factorials also turn up in calculus; for example they occur in the denominators of the terms of Taylor's formula, basically to compensate for the fact that the n-th derivative of xn is n!. Factorials are also used extensively in probability theory. Factorials can be useful to facilitate expression manipulation.
Here we present two problems based on factorials.
<br>

# Problem 1:
<br>
Given a positive integer, find the number of digits in the factorial of the number.
<br>

## Input Specification
<br>
Input will contain a single positive integer N(<10<sup>6</sup>) input from keyboard.
<br>

## Output Specification
<br>
Output must be the number of digits in the factorial of the given number.
<br>

## Sample Input and Output
<br>
Input: 5<br>
Output:2<br>
Input: 52<br>
Output: 68<br>
<br>

# Problem 2:
<br>
Find the number of zeroes at the end of factorial of a number.
<br>

## Input Specification
<br>
Input will contain a single positive integer N input from keyboard, lesser than 10<sup>9</sup>.
<br>

## Output Specification
<br>
Output must be the number of zeroes at the end of the factorial of the given number.
<br>

## Sample Input and Output
<br>
Input: 5<br>
Output:1<br>
Input: 25<br>
Output: 6<br>
