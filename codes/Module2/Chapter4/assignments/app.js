var count = document.getElementById("n");
var table = document.getElementById("table"); //get table with its style and attributes from html file
function mult() {
    var n = +(count.value); //to typecast string to integer value
    while (table.rows.length > 1) //to delete the rows 
     {
        table.deleteRow(1);
    }
    console.log(n);
    for (let i = 1; i <= n; i++) {
        // console.log(i);
        // console.log("n * "+ i + " = " + n*i);
        var row = table.insertRow(); //insert a row  
        var cell1 = row.insertCell(); //insert cell in the row
        var cell2 = row.insertCell();
        var cell3 = row.insertCell();
        cell1.innerHTML = n.toString() + " * " + i.toString(); //assign value to the cell
        cell2.innerHTML = " = ";
        cell3.innerHTML = (n * i).toString();
    }
}
//# sourceMappingURL=app.js.map